var scheduler = (function () {
  var _events
    , _container
    , _maxWidth
    , _eventTemplate
    , blankEventData = {
      name: 'Sample event',
      location: 'Location'
    }
    ;

  function bindDomLoaded(callback) {
    if (window.addEventListener) {
      window.addEventListener('DOMContentLoaded', callback, false);
    } else {
      window.attachEvent('onload', callback);
    }
  }

  function getLayout(events) {
    var items = events || _events
      , result = []
      , stack = []
      , tempEnd = 0
      ; 

    for (var i = 0, evl = items.length; i < evl; i++) {
      var item = items[i]
        , isLastItem = (i + 1 == evl)
        ;
      
      item.height = item.end - item.start;
      item.top = item.start;
      item.left = 0;
      item.width = _maxWidth;

      if (!tempEnd || tempEnd > item.start) {
        stack.push(item);
        if (tempEnd < item.end) {
          tempEnd = item.end;
        }
      } 

      if (isLastItem || tempEnd < item.start) {
        if (stack.length >= 2) {
          var colsCount = 2;

          for (var j = 1, stl = stack.length; j < stl; j++) {
            if (
              stack[j - 1].end >= stack[j].end || 
              stack[j - 1].start == stack[j].start
            ) {
              colsCount += 1;
            }
          }
          
          for (var j = 0, stl = stack.length; j < stl; j++) {
            stack[j].width = _maxWidth / colsCount;

            if (j < colsCount) {
              stack[j].left = stack[j].width * j;
              continue;
            }

            for (var k = 1; k < colsCount + 1; k++) {
              if (stack[j].start > stack[j - k].end) {
                stack[j].left = stack[j].width * (j - k);
                break;
              }
            }
          }
        }

        stack = [item];
        tempEnd = item.end;
      }
    }

    return items;
  }

  function renderEvents(events) {
    var items = events || _events
      , fragment = document.createDocumentFragment()
      ;

    for (var i = 0, evl = _events.length; i < evl; i++) {
      var item = _events[i]
        , itemBlank = _eventTemplate.cloneNode(true)
        , eventName = item.name || blankEventData.name
        , eventLocation = item.location || blankEventData.location
        ;

      itemBlank.style.top = item.top + 'px';
      itemBlank.style.left = item.left + 'px';
      itemBlank.style.width = item.width + 'px';
      itemBlank.style.height = item.height + 'px';

      itemBlank.querySelector('.event-name').innerHTML = eventName;
      itemBlank.querySelector('.event-location').innerHTML = eventLocation;

      fragment.appendChild(itemBlank);
    }

    _container.appendChild(fragment);
  }

  function init(params) {
    _events = params.events || [];

    _events.sort(function (a, b) {
      if (a.start < b.start)
         return -1;
      if (a.start > b.start)
        return 1;
      return 0;
    });    

    bindDomLoaded(function () {
      var eventTemplate = document.getElementById(params.eventTemplateID);
      
      eventTemplate.removeAttribute('id');
      eventTemplate.parentNode.removeChild(eventTemplate);

      _container = document.getElementById(params.containerID);
      _maxWidth = params.containerWidth;
      _eventTemplate = eventTemplate.cloneNode(true);

      if (!_events.length) {
        console.info('Events array are empty');
      }

      getLayout();
      renderEvents();
    });
  }

  return {
    init: init,
    getLayout: getLayout
  }
}());